#include <iostream>
using namespace std;

int main() {
	cout << "Hello World!" << endl;
	doNotCallHelp();
	callHelp();	
	cout << "Yello" << endl;
	cout << "This will not conflict" << endl;
	return 0;
}

void callHelp() {
	cout << "Call Help" << endl;
}

void doNotCallHelp() {
	cout << "Not calling help" << endl;
}

void anotherUselessFunction() {
	cout << "Useless function" << endl;
}

void anotherUselessFunction2() {
	cout << "Another useless function2" << endl;
	cout << "What the fuck is going on" << endl;
}
